A personal blog running on Heroku [here](http://kevin-smith.herokuapp.com).

Based on the Nesta [clean theme](https://github.com/rwdaigle/nesta-theme-clean).